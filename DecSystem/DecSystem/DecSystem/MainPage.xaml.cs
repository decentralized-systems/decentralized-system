﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;
using DecSystem.Models;

namespace DecSystem
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        public void ToItems(object sender, EventArgs args)
        {
            Navigation.PushAsync(new ItemPage());
        }
        public void ToGame(object sender, EventArgs args)
        {
            Navigation.PushAsync(new GamePage());
        }
    }
}
