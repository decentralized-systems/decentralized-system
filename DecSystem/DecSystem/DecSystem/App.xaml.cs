﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using DecSystem.Models;
using DecSystem.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DecSystem
{
    public partial class App : Application
    {
        static ItemDatabase database;
        static string authToken;
        static RestService restService;
        static int currentUserId;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage (new LoginPage());
        }
        public static int CurrentUserId { get; set; }
        public static string AuthToken { get; set; }

        public static ItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new ItemDatabase(
                      Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DecSQLite.db3"));
                }
                return database;
            }
        }
        public static RestService RestService
        {
            get
            {
                if (restService == null)
                {
                    restService = new RestService();
                }
                return restService;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
