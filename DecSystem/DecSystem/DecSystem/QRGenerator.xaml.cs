﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;


namespace DecSystem
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QRGenerator : ContentPage
	{
        string Withdrawing;
        string Accepting;
		public QRGenerator (string withdrawing, string accepting)
		{
			InitializeComponent ();
            BarcodeImageView.BarcodeValue = withdrawing;
            BarcodeImageView.IsVisible = true;
            Withdrawing = withdrawing;
            Accepting = accepting;
		}
        public void OnContinue(object sender, EventArgs args)
        {
            // If we don't have opponent's QR code scanned, then we need to scan it
            if (Accepting == "")
            {
                var scanPage = new ZXingScannerPage();
                scanPage.OnScanResult += (result) =>
                {
                    // Stop scanning
                    scanPage.IsScanning = false;

                    // Pop the page and show the result
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Navigation.PushAsync(new ExchangeViewPage(Withdrawing, result.Text));
                    });
                };
                Navigation.PushAsync(scanPage);
            }
            else
            {
                Navigation.PushAsync(new ExchangeViewPage(Withdrawing, Accepting));
            }
        }
    }
}