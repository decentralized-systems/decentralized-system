﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DecSystem.Models;

namespace DecSystem
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ExchangeViewPage : ContentPage
	{
        public IList<Item> withdrawList { get; private set; }
        public IList<Item> acceptList { get; private set; }


		public ExchangeViewPage (string withdrawing, string accepting)
		{
			InitializeComponent ();
            withdrawList = parseItemString(withdrawing);
            acceptList = parseItemString(accepting);
            withdrawListView.ItemsSource = withdrawList;
            acceptListView.ItemsSource = acceptList;
		}
        /// <summary>
        /// Method that deletes "given" items, and saves "accepted" items instead
        /// </summary>
        public void FinishExchange(object sender, EventArgs args)
        {
            foreach(Item item in withdrawList)
            {
                App.Database.DeleteItemAsync(item);
            }
            foreach(Item item in acceptList)
            {
                App.Database.SaveItemAsync(item);
            }
            // Removing pages from the navigation stack to pop directly to items page
            for (var counter = 1; counter < 3; counter++)
            {
                Navigation.RemovePage(Navigation.NavigationStack[Navigation.NavigationStack.Count - 2]);
            }
            Navigation.PopAsync();
        }
        /// <summary>
        /// Parse items from QR code string into Item objects
        /// </summary>
        /// <param name="itemString">QR code string. Parameters separated by commas, items separated by semicolons</param>
        /// <returns>A List of Items based on the QR code string</returns>
        public List<Item> parseItemString(string itemString)
        {
            List<Item> itemList = new List<Item>();
            var items = itemString.Split(';');
            //Remove the last (empty) element of the array
            Array.Resize(ref items, items.Length - 1);
            foreach(string item in items)
            {
                var itemParameters = item.Split(',');
                Item itemObject = new Item();
                itemObject.Id = Int32.Parse(itemParameters[0]);
                itemObject.Name = itemParameters[1];
                itemObject.Price = Int32.Parse(itemParameters[2]);
                itemObject.Color = itemParameters[3];
                itemList.Add(itemObject);
            }
            return itemList;
        }
	}
}