﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.AspNetCore.SignalR.Client;
using DecSystem.Models;
using Microsoft.Extensions.Logging.Debug;

namespace DecSystem
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GamePage : ContentPage
	{
        HubConnection hubConnection;
        int userId;
        Coordinates previousCellCoordinates;

		public GamePage ()
		{
			InitializeComponent ();
            hubConnection = new HubConnectionBuilder().WithUrl("https://forever-game.herokuapp.com/gameHub", options => 
            {
                options.AccessTokenProvider = () => Task.FromResult(App.AuthToken);
            }).Build();

        }

        // We need to connect whenever the page is invoked, and disconnect whenever it shifts into background/another page
        protected async override void OnAppearing()
        {
            await this.Connect();
            //Recieve gamestate updates
            hubConnection.On<GameState>("GetState", UpdateGameState);
            hubConnection.On("TestMessage", (string hello) => 
            {
                TestMessage(hello);
            });

        }
        public void TestMessage(string hello)
        {
            DisplayAlert("TestMessage", hello, "Ok");
        }
        protected async override void OnDisappearing()
        {
            await this.Disconnect();
        }

        public async Task Connect()
        {
            try
            {
                await hubConnection.StartAsync();
            }
            catch (Exception e)
            {
                await DisplayAlert("Exception", "Scary Exception happened: " + e.Message, "Ok");
            }
        }
        public async Task Disconnect()
        {
            await hubConnection.StopAsync();
        }
        public async void moveUp(object sender, EventArgs args)
        {
            await hubConnection.InvokeAsync("Move", userId, "Up");

        }
        public async void moveDown(object sender, EventArgs args)
        {
            try
            {
                await hubConnection.InvokeAsync("move", userId, "Down");

            }
            catch (Exception e)
            {
                await DisplayAlert("Exception", e.Message, "ok");
            }
        }
        public async void moveLeft(object sender, EventArgs args)
        {
            try
            {
                await hubConnection.SendAsync("Move", userId, "Left");
            }
            catch (Exception e)
            {
                await DisplayAlert("Exception", e.Message, "ok");
            }
        }
        public async void moveRight(object sender, EventArgs args)
        {
            try
            {
                await hubConnection.InvokeAsync("Move", userId, "Right");
            }
            catch (Exception e)
            {
                await DisplayAlert("Exception", e.Message, "ok");
            }
        }
        public async void grabItem(object sender, EventArgs args)
        {
            try
            {
                //We need confirmation about grabbed item
                Item grabbedItem = await hubConnection.InvokeAsync<Item>("GrabItem", userId);
                if (grabbedItem != null)
                {
                    await App.Database.SaveItemAsync(grabbedItem);
                }
            }
            catch (Exception e)
            {
                await DisplayAlert("Exception", e.Message, "ok");
            }
        }

        /// <summary>
        /// Updating the game state visible to client (through labels)
        /// </summary>
        /// <param name="state"></param>
        public void UpdateGameState(GameState state)
        {
            coordinateLabel.Text = "";
            foreach (GameCellResponse cell in state.Field)
            {

                if (cell.Player.Id == userId)
                {
                    if (cell.Coordinates == previousCellCoordinates)
                    {
                        coordinateLabel.Text += "Ouch! You hit a wall./n";
                    }
                    if (cell.Item != null)
                    {
                        itemLabel.Text = "You see " + cell.Item.Colors + " " + cell.Item.Name + " here!";
                    }
                    else
                    {
                        itemLabel.Text = "You see no items here";
                    }
                    coordinateLabel.Text += "You are at cell " + cell.Coordinates.X + ":" + cell.Coordinates.Y;
                    previousCellCoordinates = cell.Coordinates;
                }
            }
            foreach (KeyValuePair<int, int> stat in state.Statistic)
            {
                highScoreLabel.Text += stat.Key.ToString() + ": " + stat.Value.ToString() + "/n";
            }
        }
    }
}