﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing.Net.Mobile.Forms;
using DecSystem.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DecSystem
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ItemPage : ContentPage
	{
        public IList<Item> items { get; private set; }

		public ItemPage ()
		{
			InitializeComponent ();
            //Generating some mock items
            Item newItem = new Item();
            newItem.Name = "Toy Car";
            newItem.Color = "Green";
            newItem.Id = 3;
            newItem.Price = 90;
            App.Database.SaveItemAsync(newItem);
            Item newItem2 = new Item();
            newItem2.Name = "Sweet Candy";
            newItem2.Color = "Red";
            newItem2.Id = 4;
            newItem2.Price = 20;
            App.Database.SaveItemAsync(newItem2);
            //Binding itemlist from database to CollectionView
            items = App.Database.GetItemsAsync().Result;
            itemList.ItemsSource = items;

            
		}
        /// <summary>
        /// Refreshes the page every time it is invoked
        /// </summary>
        protected override void OnAppearing()
        {
            items = App.Database.GetItemsAsync().Result;
            itemList.ItemsSource = items;

        }
        private string GenerateQRString(IList<object> itemList)
        {
            string qrString = "";
            foreach (Item item in itemList)
            {
                qrString += item.Id.ToString() + ",";
                qrString += item.Name + ",";
                qrString += item.Price.ToString() + ",";
                qrString += item.Color + ";";
            }
            return qrString;
        }
        /// <summary>
        /// Item exchange protocol for the master. 
        /// For master, exchange protocol consists of showing generated QR code, then scanning QR code itself
        /// For slave, it's the opposite
        /// </summary>
        public void OnExchangeMaster(object sender, EventArgs args)
        {
            var selectedItems = itemList.SelectedItems;
            if (selectedItems.Count != 0)
            {
                // We need to generate a QR code with all item values. To do this, we need to concatenate them into a string
                var qrString = GenerateQRString(selectedItems);
                Navigation.PushAsync(new QRGenerator(qrString, ""));
            }
            else
            {
                DisplayAlert("Not enough Items", "It appears you did not select any items. Please select at least one item", "OK");
            }

        }
        public void OnExchangeSlave(object sender, EventArgs args)
        {
            var selectedItems = itemList.SelectedItems;
            if (selectedItems.Count != 0)
            {
                var qrString = GenerateQRString(selectedItems);
                var scanPage = new ZXingScannerPage();
                scanPage.IsScanning = true;
                scanPage.OnScanResult += (result) =>
                {
                    // Stop scanning
                    scanPage.IsScanning = false;

                    // Pop the page and show the result
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Navigation.PushAsync(new QRGenerator(qrString, result.Text));
                    });
                };
                Navigation.PushAsync(scanPage);
            }
            else
            {
                DisplayAlert("Not enough Items", "It appears you did not select any items. Please select at least one item", "OK");
            }
        }
        //Deleting selected items
        public async void OnDelete(object sender, EventArgs args)
        {
            var selectedItems = itemList.SelectedItems;
            foreach(Item item in selectedItems)
            {
                await App.Database.DeleteItemAsync(item);
            }
        }
    }
}