﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.Threading.Tasks;

namespace DecSystem.Models
{
    /// <summary>
    /// Class for initializing and managing SQLite database
    /// </summary>
    public class ItemDatabase
    {
        readonly SQLiteAsyncConnection database;

        public ItemDatabase(string dbPath)
        {
            // Should only create new database when installed because database is static
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Item>().Wait();
        }
        public Task<List<Item>> GetItemsAsync()
        {
            return database.Table<Item>().ToListAsync();
        }
        /// <summary>
        /// Method for getting items, that are not syncrhonized with backend database
        /// </summary>
        /// <returns></returns>
        public Task<List<Item>> GetUnsincItemIdsAsync()
        {
            return database.QueryAsync<Item>("SELECT Id FROM [Item] WHERE [is_synch] = false");
        }

        public Task<Item> GetItemAsync(int id)
        {
            return database.Table<Item>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(Item item)
        {
            var returnStuff = this.GetItemAsync(item.Id).Result;
            if (returnStuff != null)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(Item item)
        {
            return database.DeleteAsync(item);
        }
    }
}
