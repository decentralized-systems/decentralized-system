﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecSystem.Models
{
    public class Constants
    {
        public static string ApiUrl = "https://forever-game.herokuapp.com/api/";
        public static bool IsDev = true;
    }
}
