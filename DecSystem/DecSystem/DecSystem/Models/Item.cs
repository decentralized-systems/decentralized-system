﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace DecSystem.Models
{
    [Table("Item")]
    public class Item
    {
        int id = 0;
        string name;
        int price;
        string color;
        bool is_synch = false;

        [PrimaryKey]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Price
        {
            get { return price; }
            set { price = value; }
        }
        public string Color
        {
            get { return color; }
            set { color = value; }
        }
        public bool Is_Synch
        {
            get { return is_synch;  }
            set { is_synch = value; }
        }
    }

    public enum ItemColor
    {
        Red, Green, Blue, Orange
    }
}
