﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace DecSystem.Models
{
    public class RestService
    {
        public HttpClient client;

        public RestService()
        {
            client = new HttpClient();
        }

        public async Task<HttpResponseMessage> AuthUser(User user, string mode)
        {
            var uri = "";
            if (mode == "register")
            {
                uri = Constants.ApiUrl + "users/register";
            }
            else if (mode == "signIn")
            {
                uri = Constants.ApiUrl + "users/auth";
            }
            else
            {
                throw new System.ArgumentException("Mode has to be either register or signIn", "original");
            }
            var userObject = new { login = user.username, password = user.password};
            var json = JsonConvert.SerializeObject(userObject);
            var request = new HttpRequestMessage();
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(uri, content);
            return response;
        }
        public async Task<HttpResponseMessage> SyncItems()
        {
            string uri = Constants.ApiUrl + "users/sync";
            var itemIdList = /*App.Database.GetUnsincItemIdsAsync();*/new List<int>();
            itemIdList.Add(2);
            itemIdList.Add(3);
            HttpRequestMessage request = new HttpRequestMessage();
            var sendObject = new { userId = App.CurrentUserId, itemIds = itemIdList };
            var json = JsonConvert.SerializeObject(sendObject);
            HttpContent content = new StringContent(json);
            HttpResponseMessage response = await client.PostAsync(uri, content);
            return response;

        }

        //public async Task<HttpResponseMessage> SyncItems(List<Item> itemList)
        //{
        //    foreach (Item item in itemList)
        //    {

        //    }
        //}
    }

}
