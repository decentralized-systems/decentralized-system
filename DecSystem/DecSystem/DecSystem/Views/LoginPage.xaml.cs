﻿using DecSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace DecSystem.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        public async void SignInProcedure(object sender, EventArgs e)
        {
            User user = new User(Entry_Username.Text, Entry_Password.Text);
            if (user.CheckInformation())
            {
                var response = await App.RestService.AuthUser(user, "signIn");
                var responseMessage = await response.Content.ReadAsStringAsync();
                JObject responseObject = JsonConvert.DeserializeObject<JObject>(responseMessage);

                if (response.IsSuccessStatusCode)
                {
                    App.AuthToken = responseObject["response"]["token"].ToString();
                    App.CurrentUserId = Int32.Parse(responseObject["response"]["userId"].ToString());
                    await App.RestService.SyncItems();
                    await DisplayAlert("Login", "Login Success", "Ok");
                    await Navigation.PushAsync(new MainPage());

                }
                else
                {
                    await DisplayAlert("Login", responseObject["error"]["message"].ToString(), "Ok");
                }
            }
            else
            {
                await DisplayAlert("Login", "Login not correct, empty username or password", "Ok");
            }
                
        }

        public async void RegisterProcedure(object sender, EventArgs e)
        {
            User user = new User(Entry_Username.Text, Entry_Password.Text);
            var response = await App.RestService.AuthUser(user, "register");
            string responseMessage = await response.Content.ReadAsStringAsync();
            JObject responseObject = JsonConvert.DeserializeObject<JObject>(responseMessage);

            if (user.CheckInformation())
            {
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync();
                    await DisplayAlert("Register", "Login Success", "Ok");
                    await Navigation.PushAsync(new ItemPage());

                }
                else
                {
                    await DisplayAlert("Register", responseObject["error"]["message"].ToString(), "Ok");
                }
            }
                else
                {
                    await DisplayAlert("Register", "Registration had failed, empty username or password", "Ok");
                }
        }
        public void ToItems(object sender, EventArgs args)
        {
            Navigation.PushAsync(new ItemPage());
        }
    }
}